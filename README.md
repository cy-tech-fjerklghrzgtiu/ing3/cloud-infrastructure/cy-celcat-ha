# Cy-Celcat High Availability

Based on one of my personal project : [CY-Celcat](https://github.com/Obito1903/CY-celcat/tree/ha)

This repo contain the High Availability dependencies and was created for an aseignment in my Cloud-Infrastructure class.

Port 80 need to be open on the host system. If not you'll just need to change `url` in [celcat.json](./celcat.json) and the `traefik` port in [docker-compose.yaml](docker-compose.yaml) accordingly

![Alt text](arch.drawio.svg)

## Config

1. Put your celcat username and password in the [celcat.json](./celcat.json) file
2. Add the calendars to scrap in the `groups`  section of the [celcat.json](./celcat.json) like so :


```jsonc
    // ...
    "groups": {
        "pau": [
            {
                "name": "ING3ICC",
                // Id is found found in the fid query parameter of the original calendar page
                "id": "22014815"
            }
        ]
    }
    // ...
```

### Find the calendar id

1. Go to the official [CY celcat](https://services-web.cyu.fr/calendar/LdapLogin) :

2. Once Signed-in the Id is un the url of the page (For student it correspond to the "numéro  etudiant").
![Alt text](image.png)
## URLs

__Traefik (LoadBalancer) dashboard__ :

- [http://traefik.localhost:9000/dashboard](http://traefik.localhost:9000/dashboard)

__Celcat__ :

- [http://celcat.localhost](http://celcat.localhost)

## Possible future imporvement

- Split Celcat Querier and Http server
  - The Querier would store calendars into the Redis cache and The server would get calendars from Redis and send it to the client.

![Alt text](planned_arch.drawio.svg)
